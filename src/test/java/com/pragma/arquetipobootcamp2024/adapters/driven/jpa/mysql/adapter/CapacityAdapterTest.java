package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.adapter;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.CapacityEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.TechnologyEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper.ICapacityEntityMapper;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.ICapacityRepository;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.ITechnologyRepository;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
class CapacityAdapterTest {

    @Mock
    private ICapacityRepository capacityRepository;
    @Mock
    private ICapacityEntityMapper capacityEntityMapper;
    @Mock
    private ITechnologyRepository technologyRepository;

    @InjectMocks
    private CapacityAdapter capacityAdapter;

    Technology technology1 = new Technology(1L, "Spring", "Description one");
    Technology technology2 = new Technology(2L, "Java", "Description two");
    Technology technology3 = new Technology(3L, "Angular", "Description three");
    Technology technology4 = new Technology(4L, "Boostrap", "Description four");

    List<Technology> technologies = Arrays.asList(technology1, technology2, technology3, technology4);

    TechnologyEntity technologyEntity1 = new TechnologyEntity(1L, "Spring", "Description one");
    TechnologyEntity technologyEntity2 = new TechnologyEntity(2L, "Java", "Description two");
    TechnologyEntity technologyEntity3 = new TechnologyEntity(3L, "Angular", "Description three");
    TechnologyEntity technologyEntity4 = new TechnologyEntity(4L, "Boostrap", "Description four");

    List<TechnologyEntity> technologiesEntity = Arrays.asList(technologyEntity1, technologyEntity2,
            technologyEntity3, technologyEntity4);

    Capacity capacity = new Capacity(1L,"Backend Developer",
            "Description here", technologies);

    CapacityEntity capacityEntity = new CapacityEntity(1L,"Backend Developer",
            "Description here", technologiesEntity);

    @Test
    @DisplayName("Should call capacityRepository.save() once")
    void saveCapacity() {
        given(capacityRepository.findByCapacityName("Backend Developer")).willReturn(Optional.empty());
        given(technologyRepository.findById(1L)).willReturn(Optional.ofNullable(technologyEntity1));
        given(technologyRepository.findById(2L)).willReturn(Optional.ofNullable(technologyEntity2));
        given(technologyRepository.findById(3L)).willReturn(Optional.ofNullable(technologyEntity3));
        given(technologyRepository.findById(4L)).willReturn(Optional.ofNullable(technologyEntity4));

        given(capacityEntityMapper.toEntity(capacity)).willReturn(capacityEntity);

        capacityAdapter.saveCapacity(capacity);

        verify(capacityRepository).save(capacityEntity);
    }

    @Test
    void getCapacity() {
        given(capacityRepository.findByCapacityName("Backend Developer")).
                willReturn(Optional.ofNullable(capacityEntity));
        given(capacityEntityMapper.toModel(capacityEntity)).willReturn(capacity);

        Capacity result = capacityAdapter.getCapacity("Backend Developer");

        Assertions.assertEquals(capacity, result);
    }

    @Test
    void getAllCapacities() {
        Pageable pagination = PageRequest.of(1, 2, Sort.by("capacityName").ascending());

        CapacityEntity capacityEntity2 = new CapacityEntity(2L,"Frontend Developer",
                "Description here", technologiesEntity);
        CapacityEntity capacityEntity3 = new CapacityEntity(3L,"Mobile Backend Developer",
                "Description here", technologiesEntity);
        CapacityEntity capacityEntity4 = new CapacityEntity(4L,"Java Backend Developer",
                "Description here", technologiesEntity);
        List<CapacityEntity> capacityEntitiesOrder =  Arrays.asList(capacityEntity4, capacityEntity3);

        Capacity capacity2 = new Capacity(2L,"Frontend Developer",
                "Description here", technologies);
        Capacity capacity3 = new Capacity(3L,"Mobile Backend Developer",
                "Description here", technologies);
        Capacity capacity4 = new Capacity(4L,"Java Backend Developer",
                "Description here", technologies);
        List<Capacity> capacityOrder =  Arrays.asList(capacity4, capacity3);

        given(capacityRepository.findAll(pagination)).willReturn(new PageImpl<>(capacityEntitiesOrder));
        given(capacityEntityMapper.toModelList(capacityEntitiesOrder)).willReturn(capacityOrder);

        List<Capacity> result = capacityAdapter.getAllCapacities(1, 2, true, "name");

        Assertions.assertEquals(capacityOrder, result);
    }
}