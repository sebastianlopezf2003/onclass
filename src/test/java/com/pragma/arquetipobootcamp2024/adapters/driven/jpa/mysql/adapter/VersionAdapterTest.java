package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.adapter;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.BootcampEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.CapacityEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.TechnologyEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.VersionEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper.IVersionEntityMapper;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.IBootcampRepository;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.IVersionRepository;
import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import com.pragma.arquetipobootcamp2024.domain.model.Version;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class VersionAdapterTest {

    @Mock
    private IVersionRepository versionRepository;
    @Mock
    private IVersionEntityMapper versionEntityMapper;
    @Mock
    private IBootcampRepository bootcampRepository;

    @InjectMocks
    private VersionAdapter versionAdapter;

    @Test
    @DisplayName("Should call versionRepository.save() once")
    void saveVersion() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        LocalDate date1 = LocalDate.of(2023, 2, 11 );
        LocalDate date2 = LocalDate.of(2023, 6, 27 );

        Version version = new Version(1L, bootcamp, 50L, date1, date2);

        List<TechnologyEntity> technologiesEntity = Arrays.asList(
                new TechnologyEntity(1L, "Technology1", "Description"),
                new TechnologyEntity(2L, "Technology2", "Description"),
                new TechnologyEntity(3L, "Technology3", "Description")
        );

        List<CapacityEntity> capacitiesEntity = List.of(
                new CapacityEntity(1L, "Capacity1",
                        "Description", technologiesEntity)
        );

        BootcampEntity bootcampEntity = new BootcampEntity(1L, "Bootcamp1",
                "Description", capacitiesEntity);

        VersionEntity versionEntity = new VersionEntity(1L, 50L, date1, date2,
                bootcampEntity);

        given(bootcampRepository.findById(1L)).willReturn(Optional.of(bootcampEntity));
        given(versionEntityMapper.toEntity(version)).willReturn(versionEntity);

        versionAdapter.saveVersion(version);

        verify(versionRepository).save(versionEntity);
    }

    @Test
    @DisplayName("Should return a Version list correctly")
    void getAllVersions() {
        Pageable pagination = PageRequest.of(0, 2, Sort.by("maxQuota"));

        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        LocalDate date1 = LocalDate.of(2023, 2, 11 );
        LocalDate date2 = LocalDate.of(2023, 6, 27 );

        Version version1 = new Version(1L, bootcamp, 50L, date1,date2);
        Version version2 = new Version(2L, bootcamp, 40L, date1,date2);
        Version version3 = new Version(3L, bootcamp, 70L, date1,date2);

        List<Version> versions = Arrays.asList(version2, version1, version3);

        List<TechnologyEntity> technologiesEntity = Arrays.asList(
                new TechnologyEntity(1L, "Technology1", "Description"),
                new TechnologyEntity(2L, "Technology2", "Description"),
                new TechnologyEntity(3L, "Technology3", "Description")
        );

        List<CapacityEntity> capacitiesEntity = List.of(
                new CapacityEntity(1L, "Capacity1",
                        "Description", technologiesEntity)
        );

        BootcampEntity bootcampEntity = new BootcampEntity(1L, "Bootcamp1",
                "Description", capacitiesEntity);

        VersionEntity versionEntity1 = new VersionEntity(1L, 50L, date1, date2,
                bootcampEntity);
        VersionEntity versionEntity2 = new VersionEntity(2L, 40L, date1, date2,
                bootcampEntity);
        VersionEntity versionEntity3 = new VersionEntity(3L, 70L, date1, date2,
                bootcampEntity);

        List<VersionEntity> versionEntities = Arrays.asList(versionEntity2, versionEntity1,
                versionEntity3);

        given(versionRepository.findAll(pagination)).willReturn(new PageImpl<>(versionEntities));
        given(versionEntityMapper.toModelList(versionEntities)).willReturn(versions);

        List<Version> result = versionAdapter.getAllVersions(0, 2, true,
                "maxQuota");

        Assertions.assertEquals(versions, result);
    }

    @Test
    @DisplayName("Should return a Version list correctly")
    void getAllBootcampVersions() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        LocalDate date1 = LocalDate.of(2023, 2, 11 );
        LocalDate date2 = LocalDate.of(2023, 6, 27 );

        Version version1 = new Version(1L, bootcamp, 50L, date1,date2);
        Version version2 = new Version(2L, bootcamp, 40L, date1,date2);
        Version version3 = new Version(3L, bootcamp, 70L, date1,date2);

        List<Version> versions = Arrays.asList(version2, version1, version3);

        List<TechnologyEntity> technologiesEntity = Arrays.asList(
                new TechnologyEntity(1L, "Technology1", "Description"),
                new TechnologyEntity(2L, "Technology2", "Description"),
                new TechnologyEntity(3L, "Technology3", "Description")
        );

        List<CapacityEntity> capacitiesEntity = List.of(
                new CapacityEntity(1L, "Capacity1",
                        "Description", technologiesEntity)
        );

        BootcampEntity bootcampEntity = new BootcampEntity(1L, "Bootcamp1",
                "Description", capacitiesEntity);

        VersionEntity versionEntity1 = new VersionEntity(1L, 50L, date1, date2,
                bootcampEntity);
        VersionEntity versionEntity2 = new VersionEntity(2L, 40L, date1, date2,
                bootcampEntity);
        VersionEntity versionEntity3 = new VersionEntity(3L, 70L, date1, date2,
                bootcampEntity);

        List<VersionEntity> versionEntities = Arrays.asList(versionEntity2, versionEntity1,
                versionEntity3);

        given(versionRepository.findAllByBootcampBootcampName("Bootcamp1")).willReturn(versionEntities);
        given(versionEntityMapper.toModelList(versionEntities)).willReturn(versions);

        List<Version> result = versionAdapter.getAllBootcampVersions("Bootcamp1");

        Assertions.assertEquals(versions, result);
    }
}