package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.adapter;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.TechnologyEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper.ITechnologyEntityMapper;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.ITechnologyRepository;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TechnologyAdapterTest {

    @Mock
    private ITechnologyRepository technologyRepository;
    @Mock
    private ITechnologyEntityMapper technologyEntityMapper;

    @InjectMocks
    private TechnologyAdapter technologyAdapter;
    TechnologyEntity tec = new TechnologyEntity();
    TechnologyEntity technologyEntity1 = new TechnologyEntity(1L, "Spring", "Description one");
    TechnologyEntity technologyEntity2 = new TechnologyEntity(2L, "Java", "Description two");
    TechnologyEntity technologyEntity3 = new TechnologyEntity(3L, "Angular", "Description three");
    TechnologyEntity technologyEntity4 = new TechnologyEntity(4L, "Boostrap", "Description four");

    Technology technology1= new Technology(1L, "Spring", "Description one");
    Technology technology2 = new Technology(2L, "Java", "Description two");
    Technology technology3 = new Technology(3L, "Angular", "Description three");
    Technology technology4 = new Technology(4L, "Boostrap", "Description four");

    @Test
    @DisplayName("Should call technologyRepository.save() once")
    void saveTechnology() {
        given(technologyRepository.findByName("Spring")).willReturn(Optional.empty());
        given(technologyEntityMapper.toEntity(technology1)).willReturn(technologyEntity1);

        technologyAdapter.saveTechnology(technology1);
        verify(technologyRepository).save(technologyEntity1);
    }

    @Test
    @DisplayName("Should return a Technology correctly")
    void getTechnology() {
        given(technologyRepository.findByNameContaining("Spring")).willReturn(Optional.of(technologyEntity1));
        given(technologyEntityMapper.toModel(technologyEntity1)).willReturn(technology1);

        Technology result = technologyAdapter.getTechnology("Spring");

        Assertions.assertEquals(technology1, result);
    }

    @Test
    @DisplayName("Should return a Technology list correctly")
    void getAllTechnologies() {
        List<TechnologyEntity> technologyEntities = new ArrayList<>();
        technologyEntities.add(technologyEntity1);
        technologyEntities.add(technologyEntity2);
        technologyEntities.add(technologyEntity3);
        technologyEntities.add(technologyEntity4);

        List<Technology> technologies = new ArrayList<>();
        technologies.add(technology1);
        technologies.add(technology2);
        technologies.add(technology3);
        technologies.add(technology4);

        Pageable pagination = PageRequest.of(0, 5, Sort.by("name"));

        given(technologyRepository.findAll(pagination)).willReturn(new PageImpl<>(technologyEntities));
        given(technologyEntityMapper.toModelList(technologyEntities)).willReturn(technologies);

        List<Technology> result = technologyAdapter.getAllTechnologies(0, 5, true);

        Assertions.assertEquals(technologies, result);
    }
}