package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.adapter;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.BootcampEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.CapacityEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.TechnologyEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper.IBootcampEntityMapper;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.IBootcampRepository;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.ICapacityRepository;
import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BootcampAdapterTest {
    @Mock
    private IBootcampRepository bootcampRepository;
    @Mock
    private IBootcampEntityMapper bootcampEntityMapper;
    @Mock
    private ICapacityRepository capacityRepository;

    @InjectMocks
    private BootcampAdapter bootcampAdapter;

    @Test
    @DisplayName("Should call bootcampRepository.save() once")
    void saveBootcamp() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        List<TechnologyEntity> technologiesEntity = Arrays.asList(
                new TechnologyEntity(1L, "Technology1", "Description"),
                new TechnologyEntity(2L, "Technology2", "Description"),
                new TechnologyEntity(3L, "Technology3", "Description")
        );

        CapacityEntity capacityEntity = new CapacityEntity(1L, "Capacity1",
                "Description", technologiesEntity);

        List<CapacityEntity> capacitiesEntity = List.of(
                capacityEntity
        );

        BootcampEntity bootcampEntity = new BootcampEntity(1L, "Bootcamp1",
                "Description", capacitiesEntity);

        given(bootcampRepository.findByBootcampName("Bootcamp1")).willReturn(Optional.empty());
        given(capacityRepository.findById(1L)).willReturn(Optional.of(capacityEntity));
        given(bootcampEntityMapper.toEntity(bootcamp)).willReturn(bootcampEntity);

        bootcampAdapter.saveBootcamp(bootcamp);

        verify(bootcampRepository).save(bootcampEntity);
    }

    @Test
    @DisplayName("Should return a Bootcamp correctly")
    void getBootcamp() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        List<TechnologyEntity> technologiesEntity = Arrays.asList(
                new TechnologyEntity(1L, "Technology1", "Description"),
                new TechnologyEntity(2L, "Technology2", "Description"),
                new TechnologyEntity(3L, "Technology3", "Description")
        );

        List<CapacityEntity> capacitiesEntity = List.of(
                new CapacityEntity(1L, "Capacity1",
                        "Description", technologiesEntity)
        );

        BootcampEntity bootcampEntity = new BootcampEntity(1L, "Bootcamp1",
                "Description", capacitiesEntity);

        given(bootcampRepository.findByBootcampName("Bootcamp1")).willReturn(Optional.of(bootcampEntity));
        given(bootcampEntityMapper.toModel(bootcampEntity)).willReturn(bootcamp);

        Bootcamp result = bootcampAdapter.getBootcamp("Bootcamp1");

        Assertions.assertEquals(bootcamp, result);

    }

    @Test
    @DisplayName("Should return a Bootcamp list correctly")
    void getAllBootcamps() {
        Pageable pagination = PageRequest.of(0, 2, Sort.by("bootcampName").ascending());

        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        List<Bootcamp> bootcamps = Arrays.asList(
                new Bootcamp(1L, "Bootcamp1",
                        "Description", capacities),
                new Bootcamp(1L, "Bootcamp1",
                        "Description", capacities)
        );

        List<TechnologyEntity> technologiesEntity = Arrays.asList(
                new TechnologyEntity(1L, "Technology1", "Description"),
                new TechnologyEntity(2L, "Technology2", "Description"),
                new TechnologyEntity(3L, "Technology3", "Description")
        );

        List<CapacityEntity> capacitiesEntity = List.of(
                new CapacityEntity(1L, "Capacity1",
                        "Description", technologiesEntity)
        );

        BootcampEntity bootcampEntity = new BootcampEntity(1L, "Bootcamp1",
                "Description", capacitiesEntity);

        List<BootcampEntity> bootcampEntities = Arrays.asList(
                new BootcampEntity(1L, "BootcampEntity1",
                        "Description", capacitiesEntity),
                new BootcampEntity(1L, "BootcampEntity1",
                        "Description", capacitiesEntity)
        );

        given(bootcampRepository.findAll(pagination)).willReturn(new PageImpl<>(bootcampEntities));
        given(bootcampEntityMapper.toModelList(bootcampEntities)).willReturn(bootcamps);

        List<Bootcamp> result = bootcampAdapter.getAllBootcamps(0, 2, true, "name");

        Assertions.assertEquals(bootcamps, result);
    }
}