package com.pragma.arquetipobootcamp2024.adapters.driving.http.controller;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.AddBootcampRequest;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.CapacityIdRequest;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.TechnologyIdRequest;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.BootcampResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.CapacityIdResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.TechnologyIdResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.IBootcampRequestMapper;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.IBootcampResponseMapper;
import com.pragma.arquetipobootcamp2024.domain.api.IBootcampServicePort;
import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class BootcampRestControllerAdapterTest {

    @Mock
    private IBootcampServicePort bootcampServicePort;
    @Mock
    private IBootcampRequestMapper bootcampRequestMapper;
    @Mock
    private IBootcampResponseMapper bootcampResponseMapper;

    @InjectMocks
    private BootcampRestControllerAdapter bootcampRestControllerAdapter;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp(){
        mockMvc = MockMvcBuilders.standaloneSetup(bootcampRestControllerAdapter).build();
    }

    @Test
    @DisplayName("Should return ResponseEntity.status(HttpStatus.CREATED).build() and call" +
            " bootcampServicePort).saveBootcamp() once")
    void addBootcamp(){

        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        List<TechnologyIdRequest> technologiesIdRequest = Arrays.asList(
                new TechnologyIdRequest(1L),
                new TechnologyIdRequest(2L),
                new TechnologyIdRequest(3L)
        );

        List<CapacityIdRequest> capacitiesIdRequest = Arrays.asList(
                new CapacityIdRequest(1L),
                new CapacityIdRequest(2L)
        );

        AddBootcampRequest addBootcampRequest = new AddBootcampRequest("Bootcamp1",
                "Description",capacitiesIdRequest);

        doNothing().when(bootcampServicePort).saveBootcamp(bootcamp);
        given(bootcampRequestMapper.addRequestToBootcamp(addBootcampRequest)).willReturn(bootcamp);

        ResponseEntity<Void> result = bootcampRestControllerAdapter.addBootcamp(addBootcampRequest);

        verify(bootcampServicePort).saveBootcamp(bootcamp);
    }

    @Test
    @DisplayName("Should return a  ResponseEntity<BootcampResponse> correctly")
    void getBootcamp() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        List<TechnologyIdResponse> technologiesIdResponse = Arrays.asList(
                new TechnologyIdResponse(1L, "Technology1"),
                new TechnologyIdResponse(2L, "Technology2"),
                new TechnologyIdResponse(3L, "Technology3")
        );

        List<CapacityIdResponse> capacitiesIdResponse = Arrays.asList(
                new CapacityIdResponse(1L, "Capacity1", technologiesIdResponse)
        );

        BootcampResponse bootcampResponse = new BootcampResponse(1L, "Bootcamp1",
                "Description", capacitiesIdResponse);


        given(bootcampServicePort.getBootcamp("Bootcamp1")).willReturn(bootcamp);
        given(bootcampResponseMapper.toBootcampResponse(bootcamp)).willReturn(bootcampResponse);

        ResponseEntity<BootcampResponse> result = bootcampRestControllerAdapter.getBootcamp("Bootcamp1");

        Assertions.assertEquals(ResponseEntity.ok(bootcampResponse), result);
    }

    @Test
    @DisplayName("Should return a  ResponseEntity<List<BootcampResponse>> correctly")
    void getAllBootcamps() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        List<Bootcamp> bootcamps = Arrays.asList(
                new Bootcamp(1L, "Bootcamp1",
                        "Description", capacities),
                new Bootcamp(1L, "Bootcamp1",
                        "Description", capacities)
        );

        List<TechnologyIdResponse> technologiesIdResponse = Arrays.asList(
                new TechnologyIdResponse(1L, "Technology1"),
                new TechnologyIdResponse(2L, "Technology2"),
                new TechnologyIdResponse(3L, "Technology3")
        );

        List<CapacityIdResponse> capacitiesIdResponse = List.of(
                new CapacityIdResponse(1L, "Capacity1", technologiesIdResponse)
        );

        List<BootcampResponse> bootcampResponses = Arrays.asList(
                new BootcampResponse(1L, "Bootcamp1",
                        "Description", capacitiesIdResponse),
                new BootcampResponse(2L, "Bootcamp2",
                        "Description", capacitiesIdResponse)
        );

        given(bootcampServicePort.getAllBootcamps(0, 2, true, "name")).willReturn(bootcamps);
        given(bootcampResponseMapper.toBootcampResponseList(bootcamps)).willReturn(bootcampResponses);

        ResponseEntity<List<BootcampResponse>> result = bootcampRestControllerAdapter.getAllBootcamps(0,2,
                true, "name");

        Assertions.assertEquals(ResponseEntity.ok(bootcampResponses), result);
    }
}