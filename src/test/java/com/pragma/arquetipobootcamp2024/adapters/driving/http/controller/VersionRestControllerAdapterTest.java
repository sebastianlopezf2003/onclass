package com.pragma.arquetipobootcamp2024.adapters.driving.http.controller;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.*;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.*;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.IVersionRequestMapper;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.IVersionResponseMapper;
import com.pragma.arquetipobootcamp2024.domain.api.IVersionServicePort;
import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import com.pragma.arquetipobootcamp2024.domain.model.Version;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class VersionRestControllerAdapterTest {

    @Mock
    private IVersionServicePort versionServicePort;
    @Mock
    private IVersionRequestMapper versionRequestMapper;
    @Mock
    private IVersionResponseMapper versionResponseMapper;

    @InjectMocks
    private VersionRestControllerAdapter versionRestControllerAdapter;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp(){
        mockMvc = MockMvcBuilders.standaloneSetup(versionRestControllerAdapter).build();
    }

    @Test
    @DisplayName("Should return ResponseEntity.status(HttpStatus.CREATED).build() and call " +
            "versionServicePort).saveVersion() once")
    void addVersion() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        Version version = new Version(1L, bootcamp, 50L,
                LocalDate.of(2023, 2, 11 ),
                LocalDate.of(2023, 2, 11 ));

        BootcampIdRequest bootcampIdRequest = new BootcampIdRequest(1L);

        AddVersionRequest addVersionRequest = new AddVersionRequest(bootcampIdRequest, 70L,
                LocalDate.of(2023, 2, 11 ),
                LocalDate.of(2023, 2, 11 ));

        doNothing().when(versionServicePort).saveVersion(version);
        given(versionRequestMapper.addRequestToVersion(addVersionRequest)).willReturn(version);

        ResponseEntity<Void> result = versionRestControllerAdapter.addVersion(addVersionRequest);

        Assertions.assertEquals(ResponseEntity.status(HttpStatus.CREATED).build(), result);

        verify(versionServicePort).saveVersion(version);
    }

    @Test
    @DisplayName("Should return a  ResponseEntity<VersionResponse> correctly")
    void getAllVersions() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        LocalDate date1 = LocalDate.of(2023, 2, 11 );
        LocalDate date2 = LocalDate.of(2023, 6, 27 );

        Version version1 = new Version(1L, bootcamp, 50L, date1,date2);
        Version version2 = new Version(2L, bootcamp, 40L, date1,date2);
        Version version3 = new Version(3L, bootcamp, 70L, date1,date2);

        List<Version> versions = Arrays.asList(version2, version1, version3);

        BootcampIdResponse bootcampIdResponse = new BootcampIdResponse(1L, "Bootcamp1");

        VersionResponse versionResponse1 = new VersionResponse(1L, bootcampIdResponse, 50L, date1,date2);
        VersionResponse versionResponse2 = new VersionResponse(2L, bootcampIdResponse, 40L, date1,date2);
        VersionResponse versionResponse3 = new VersionResponse(3L, bootcampIdResponse, 70L, date1,date2);

        List<VersionResponse> versionResponses = Arrays.asList(
                versionResponse2, versionResponse1, versionResponse3
        );

        given(versionServicePort.getAllVersions(0, 2, true, "maxQuota")).willReturn(versions);
        given(versionResponseMapper.toVersionResponseList(versions)).willReturn(versionResponses);

        ResponseEntity<List<VersionResponse>> result = versionRestControllerAdapter.
                getAllVersions(0, 2, true, "maxQuota");

        Assertions.assertEquals(ResponseEntity.ok(versionResponses), result);
    }

    @Test
    @DisplayName("Should return a  ResponseEntity<List<VersionResponse>> correctly")
    void getAllBootcampVersions() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        LocalDate date1 = LocalDate.of(2023, 2, 11 );
        LocalDate date2 = LocalDate.of(2023, 2, 11 );

        Version version1 = new Version(1L, bootcamp, 50L, date1,date2);
        Version version2 = new Version(2L, bootcamp, 40L, date1,date2);
        Version version3 = new Version(3L, bootcamp, 70L, date1,date2);

        List<Version> versions = Arrays.asList(version2, version1, version3);

        BootcampIdResponse bootcampIdResponse = new BootcampIdResponse(1L, "Bootcamp1");

        VersionResponse versionResponse1 = new VersionResponse(1L, bootcampIdResponse, 50L, date1,date2);
        VersionResponse versionResponse2 = new VersionResponse(2L, bootcampIdResponse, 40L, date1,date2);
        VersionResponse versionResponse3 = new VersionResponse(3L, bootcampIdResponse, 70L, date1,date2);

        List<VersionResponse> versionResponses = Arrays.asList(
                versionResponse1, versionResponse2, versionResponse3
        );

        given(versionServicePort.getAllBootcampVersions("Bootcamp1")).willReturn(versions);
        given((versionResponseMapper.toVersionResponseList(versions))).willReturn(versionResponses);

        ResponseEntity<List<VersionResponse>> result = versionRestControllerAdapter.
                getAllBootcampVersions("Bootcamp1");

        Assertions.assertEquals(ResponseEntity.ok(versionResponses), result);
     }
}