package com.pragma.arquetipobootcamp2024.adapters.driving.http.controller;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.AddTechnologyRequest;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.TechnologyResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.ITechnologyRequestMapper;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.ITechnologyResponseMapper;
import com.pragma.arquetipobootcamp2024.domain.api.ITechnologyServicePort;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TechnologyRestControllerAdapterTest {

    @Mock
    private ITechnologyServicePort technologyServicePort;
    @Mock
    private ITechnologyRequestMapper technologyRequestMapper;
    @Mock
    private ITechnologyResponseMapper technologyResponseMapper;

    @InjectMocks
    private TechnologyRestControllerAdapter technologyRestControllerAdapter;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp(){
        mockMvc = MockMvcBuilders.standaloneSetup(technologyRestControllerAdapter).build();
    }

    Technology technology1 = new Technology(1L, "Spring", "Description one");
    Technology technology2 = new Technology(2L, "Java", "Description two");
    Technology technology3 = new Technology(3L, "Angular", "Description three");
    Technology technology4 = new Technology(4L, "Boostrap", "Description four");

    AddTechnologyRequest request1 = new AddTechnologyRequest("Spring", "Description one");

    TechnologyResponse response1 = new TechnologyResponse(1L, "Spring", "Description one");
    TechnologyResponse response2 = new TechnologyResponse(2L, "Java", "Description two");
    TechnologyResponse response3 = new TechnologyResponse(3L, "Angular", "Description three");
    TechnologyResponse response4 = new TechnologyResponse(4L, "Boostrap", "Description four");

    @Test
    @DisplayName("Should show that the technology was successfully created")
    void addTechnology() {
        doNothing().when(technologyServicePort).saveTechnology(technology1);
        given(technologyRequestMapper.addRequestToTechnology(request1)).willReturn(technology1);

        ResponseEntity<Void> result = technologyRestControllerAdapter.addTechnology(request1);

        Assertions.assertEquals(ResponseEntity.status(HttpStatus.CREATED).build(), result);
    }

    @Test
    @DisplayName("Should return a technologyResponse correctly")
    void getTechnology() {
        given(technologyServicePort.getTechnology("Spring")).willReturn(technology1);
        given(technologyResponseMapper.toTechnologyResponse(technology1)).willReturn(response1);

        ResponseEntity<TechnologyResponse> result = technologyRestControllerAdapter.getTechnology("Spring");

        Assertions.assertEquals(ResponseEntity.ok(response1), result);
    }

    @Test
    @DisplayName("Should return a technologyResponse list correctly")
    void getAllTechnologies() {
        List<Technology> technologies = new ArrayList<>();
        technologies.add(technology1);
        technologies.add(technology2);
        technologies.add(technology3);
        technologies.add(technology4);

        List<TechnologyResponse> responses = new ArrayList<>();
        responses.add(response1);
        responses.add(response2);
        responses.add(response3);
        responses.add(response4);

        given(technologyResponseMapper.toTechnologyResponseList(technologies)).willReturn(responses);
        given(technologyServicePort.getAllTechnologies(0, 5, true))
                .willReturn(technologies);

        ResponseEntity<List<TechnologyResponse>> result = technologyRestControllerAdapter.
                getAllTechnologies(0, 5, true);

        Assertions.assertEquals(ResponseEntity.ok(responses), result);
    }
}