package com.pragma.arquetipobootcamp2024.adapters.driving.http.controller;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.AddCapacityRequest;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.TechnologyIdRequest;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.CapacityResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.TechnologyIdResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.ICapacityRequestMapper;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.ICapacityResponseMapper;
import com.pragma.arquetipobootcamp2024.domain.api.ICapacityServicePort;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

@ExtendWith(MockitoExtension.class)
class CapacityRestControllerAdapterTest {

    @Mock
    private ICapacityServicePort capacityServicePort;
    @Mock
    private ICapacityRequestMapper capacityRequestMapper;
    @Mock
    private ICapacityResponseMapper capacityResponseMapper;

    @InjectMocks
    private CapacityRestControllerAdapter capacityRestControllerAdapter;

    @BeforeEach
    void setUp(){
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(capacityRestControllerAdapter).build();
    }

    TechnologyIdResponse technologyIdResponse1 = new TechnologyIdResponse(1L, "Spring");
    TechnologyIdResponse technologyIdResponse2 = new TechnologyIdResponse(2L, "Java");
    TechnologyIdResponse technologyIdResponse3 = new TechnologyIdResponse(3L, "MySQL");
    TechnologyIdResponse technologyIdResponse4 = new TechnologyIdResponse(4L, "Boostrap");

    Technology technology1 = new Technology(1L, "Spring", "Description one");
    Technology technology2 = new Technology(2L, "Java", "Description two");
    Technology technology3 = new Technology(3L, "Angular", "Description three");
    Technology technology4 = new Technology(4L, "Boostrap", "Description four");

    List<TechnologyIdResponse> technologiesIdResponse = Arrays.asList(technologyIdResponse1,
            technologyIdResponse2, technologyIdResponse3, technologyIdResponse4);
    List<Technology> technologies = Arrays.asList(technology1, technology2, technology3, technology4);

    Capacity capacity = new Capacity(1L,"Backend Developer",
            "Description here", technologies);

    CapacityResponse capacityResponse = new CapacityResponse(1L, "Backend Developer",
            "Description here", technologiesIdResponse);


    @Test
    @DisplayName("should show that the capacity was successfully created")
    void addCapacity() {
        TechnologyIdRequest technologyIdRequest1 = new TechnologyIdRequest(1L);
        AddCapacityRequest addCapacityRequest = getAddCapacityRequest(technologyIdRequest1);

        doNothing().when(capacityServicePort).saveCapacity(capacity);
        given(capacityRequestMapper.addRequestToCapacity(addCapacityRequest)).willReturn(capacity);

        ResponseEntity<Void> result = capacityRestControllerAdapter.addCapacity(addCapacityRequest);

        Assertions.assertEquals(ResponseEntity.status(HttpStatus.CREATED).build(), result);
    }

    private static AddCapacityRequest getAddCapacityRequest(TechnologyIdRequest technologyIdRequest1) {
        TechnologyIdRequest technologyIdRequest2 = new TechnologyIdRequest(2L);
        TechnologyIdRequest technologyIdRequest3 = new TechnologyIdRequest(3L);
        TechnologyIdRequest technologyIdRequest4 = new TechnologyIdRequest(4L);

        List<TechnologyIdRequest> technologiesIdRequest = Arrays.asList(technologyIdRequest1, technologyIdRequest2,
                technologyIdRequest3, technologyIdRequest4);

        return new AddCapacityRequest("Backend Developer",
                "Description here", technologiesIdRequest);
    }

    @Test
    @DisplayName("should return a capacityResponse correctly")
    void getCapacity() {
        given(capacityServicePort.getCapacity("Backend Developer")).willReturn(capacity);
        given(capacityResponseMapper.toCapacityResponse(capacity)).willReturn(capacityResponse);

        ResponseEntity<CapacityResponse> result = capacityRestControllerAdapter.
                getCapacity("Backend Developer");

        Assertions.assertEquals(ResponseEntity.ok(capacityResponse), result);
    }

    @Test
    @DisplayName("Should return a capacityResponse correctly")
    void getAllCapacities() {
        Capacity capacity2 = new Capacity(2L,"Frontend Developer",
                "Description here", technologies);
        Capacity capacity3 = new Capacity(3L,"Mobile Backend Developer",
                "Description here", technologies);
        Capacity capacity4 = new Capacity(4L,"Java Backend Developer",
                "Description here", technologies);

        CapacityResponse capacityResponse2 = new CapacityResponse(2L,
                "Frontend Developer", "Description here", technologiesIdResponse);
        CapacityResponse capacityResponse3 = new CapacityResponse(3L,
                "Mobile Backend Developer", "Description here", technologiesIdResponse);
        CapacityResponse capacityResponse4 = new CapacityResponse(4L,
                "Java Backend Developer", "Description here", technologiesIdResponse);

        List<Capacity> capacities = Arrays.asList(capacity, capacity2,
                capacity3, capacity4);
        List<Capacity> capacitiesCorrect = Arrays.asList(capacity4, capacity3);

        List<CapacityResponse> capacityResponses = Arrays.asList(capacityResponse, capacityResponse2,
                capacityResponse3, capacityResponse4);
        List<CapacityResponse> capacityResponsesCorrect = Arrays.asList(capacityResponse4, capacityResponse3);

        given(capacityServicePort.getAllCapacities(1, 2, true, "name"))
                .willReturn(capacitiesCorrect);
        given(capacityResponseMapper.toCapacityResponseList(capacitiesCorrect))
                .willReturn(capacityResponsesCorrect);

        ResponseEntity<List<CapacityResponse>> result = capacityRestControllerAdapter
                .getAllCapacities(1, 2, true, "name");

        Assertions.assertEquals(ResponseEntity.ok(capacityResponsesCorrect), result);
    }
}