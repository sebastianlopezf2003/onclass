package com.pragma.arquetipobootcamp2024.domain.api.usecase;

import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import com.pragma.arquetipobootcamp2024.domain.model.Version;
import com.pragma.arquetipobootcamp2024.domain.spi.IVersionPersistencePort;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class VersionUseCaseTest {

    @Mock
    private IVersionPersistencePort versionPersistencePort;

    @InjectMocks
    private VersionUseCase versionUseCase;

    @Test
    @DisplayName("Should call versionPersistencePort.saveVersion() once")
    void saveVersion() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        LocalDate date1 = LocalDate.of(2023, 2, 11 );
        LocalDate date2 = LocalDate.of(2023, 6, 27 );

        Version version = new Version(1L, bootcamp, 50L, date1, date2);

        versionUseCase.saveVersion(version);

        verify(versionPersistencePort).saveVersion(version);
    }

    @Test
    @DisplayName("Should return a Version list correctly")
    void getAllVersions() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        LocalDate date1 = LocalDate.of(2023, 2, 11 );
        LocalDate date2 = LocalDate.of(2023, 6, 27 );

        Version version1 = new Version(1L, bootcamp, 50L, date1,date2);
        Version version2 = new Version(2L, bootcamp, 40L, date1,date2);
        Version version3 = new Version(3L, bootcamp, 70L, date1,date2);

        List<Version> versions = Arrays.asList(version2, version1, version3);

        given(versionPersistencePort.getAllVersions(0, 2, true, "maxQuota"))
                .willReturn(versions);

        List<Version> result = versionUseCase.getAllVersions(0, 2, true, "maxQuota");

        Assertions.assertEquals(versions, result);
    }

    @Test
    @DisplayName("Should return a Version list correctly")
    void getAllBootcampVersions() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        LocalDate date1 = LocalDate.of(2023, 2, 11 );
        LocalDate date2 = LocalDate.of(2023, 6, 27 );

        Version version1 = new Version(1L, bootcamp, 50L, date1,date2);
        Version version2 = new Version(2L, bootcamp, 40L, date1,date2);
        Version version3 = new Version(3L, bootcamp, 70L, date1,date2);

        List<Version> versions = Arrays.asList(version2, version1, version3);

        given(versionPersistencePort.getAllBootcampVersions("Bootcamp1")).willReturn(versions);

        List<Version> result = versionUseCase.getAllBootcampVersions("Bootcamp1");

        Assertions.assertEquals(versions, result);
    }
}