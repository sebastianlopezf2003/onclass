package com.pragma.arquetipobootcamp2024.domain.api.usecase;

import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import com.pragma.arquetipobootcamp2024.domain.spi.IBootcampPersistencePort;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BootcampUseCaseTest {

    @Mock
    private IBootcampPersistencePort bootcampPersistencePort;

    @InjectMocks
    private BootcampUseCase bootcampUseCase;

    @Test
    void saveBootcamp() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        bootcampUseCase.saveBootcamp(bootcamp);
        verify(bootcampPersistencePort).saveBootcamp(bootcamp);
    }

    @Test
    void getBootcamp() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        Bootcamp bootcamp = new Bootcamp(1L, "Bootcamp1",
                "Description", capacities);

        given(bootcampPersistencePort.getBootcamp("Bootcamp1")).willReturn(bootcamp);

        Bootcamp result = bootcampUseCase.getBootcamp("Bootcamp1");

        Assertions.assertEquals(bootcamp, result);
    }

    @Test
    void getAllBootcamps() {
        List<Technology> technologies = Arrays.asList(
                new Technology(1L, "Technology1", "Description"),
                new Technology(2L, "Technology2", "Description"),
                new Technology(3L, "Technology3", "Description")
        );

        List<Capacity> capacities = List.of(
                new Capacity(1L, "Capacity1", "Description",
                        technologies)
        );

        List<Bootcamp> bootcamps = Arrays.asList(
                new Bootcamp(1L, "Bootcamp1",
                        "Description", capacities),
                new Bootcamp(1L, "Bootcamp1",
                        "Description", capacities)
        );

        given(bootcampPersistencePort.getAllBootcamps(0, 2, true, "name"))
                .willReturn(bootcamps);

        List<Bootcamp> result = bootcampUseCase.getAllBootcamps(0, 2, true, "name");

        Assertions.assertEquals(bootcamps, result);
    }
}