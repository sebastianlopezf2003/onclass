package com.pragma.arquetipobootcamp2024.domain.api.usecase;

import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import com.pragma.arquetipobootcamp2024.domain.spi.ICapacityPersistencePort;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
class CapacityUseCaseTest {
    @Mock
    private ICapacityPersistencePort capacityPersistencePort;

    @InjectMocks
    private CapacityUseCase capacityUseCase;

    Technology technology1 = new Technology(1L, "Spring", "Description one");
    Technology technology2 = new Technology(2L, "Java", "Description two");
    Technology technology3 = new Technology(3L, "Angular", "Description three");
    Technology technology4 = new Technology(4L, "Boostrap", "Description four");

    List<Technology> technologies = Arrays.asList(technology1, technology2, technology3, technology4);

    Capacity capacity = new Capacity(1L,"Backend Developer",
            "Description here", technologies);

    @Test
    @DisplayName("Should call capacityPersistencePort.saveCapacity() method once")
    void saveCapacity() {
        capacityUseCase.saveCapacity(capacity);
        verify(capacityPersistencePort).saveCapacity(capacity);
    }

    @Test
    @DisplayName("Should return a Capacity correctly")
    void getCapacity() {
        given(capacityPersistencePort.getCapacity("Backend Developer")).willReturn(capacity);

        Capacity result = capacityUseCase.getCapacity("Backend Developer");

        Assertions.assertEquals(capacity, result);
    }

    @Test
    @DisplayName("Should return a Capacity list correctly")
    void getAllCapacities() {
        Capacity capacity2 = new Capacity(2L,"Frontend Developer",
                "Description here", technologies);
        Capacity capacity3 = new Capacity(3L,"Mobile Backend Developer",
                "Description here", technologies);
        Capacity capacity4 = new Capacity(4L,"Java Backend Developer",
                "Description here", technologies);

        List<Capacity> capacitiesOrder = Arrays.asList(capacity4, capacity3);

        given(capacityPersistencePort.getAllCapacities(1, 2, true, "name"))
                .willReturn(capacitiesOrder);

        List<Capacity> result = capacityUseCase.getAllCapacities(1, 2, true, "name");

        Assertions.assertEquals(capacitiesOrder, result);
    }
}