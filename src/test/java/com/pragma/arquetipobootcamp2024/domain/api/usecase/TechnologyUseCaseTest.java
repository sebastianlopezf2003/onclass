package com.pragma.arquetipobootcamp2024.domain.api.usecase;

import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import com.pragma.arquetipobootcamp2024.domain.spi.ITechnologyPersistencePort;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
class TechnologyUseCaseTest {

    @Mock
    private ITechnologyPersistencePort technologyPersistencePort;

    @InjectMocks
    private TechnologyUseCase technologyUseCase;

    Technology technology1 = new Technology(1L, "Spring", "Description one");
    Technology technology2 = new Technology(2L, "Java", "Description two");
    Technology technology3 = new Technology(3L, "Angular", "Description three");
    Technology technology4 = new Technology(4L, "Boostrap", "Description four");

    @Test
    @DisplayName("Should call technologyPersistencePort.saveTechnology() method once")
    void saveTechnology() {
        technologyUseCase.saveTechnology(technology1);
        verify(technologyPersistencePort).saveTechnology(technology1);
    }

    @Test
    @DisplayName("Should return a Technology correctly")
    void shouldShowTechnologyCorrectly() {
        given(technologyPersistencePort.getTechnology("Spring")).willReturn(technology1);

        Technology result = technologyUseCase.getTechnology("Spring");

        Assertions.assertEquals(technology1, result);
    }

    @Test
    @DisplayName("Should return a Technology list correctly")
    void getAllTechnologies() {
        List<Technology> technologiesOrder = Arrays.asList(technology3, technology4,technology2, technology1);

        given(technologyPersistencePort.getAllTechnologies(0, 5, true)).willReturn(technologiesOrder);

        List<Technology> result = technologyUseCase.getAllTechnologies(0, 5, true);

        Assertions.assertEquals(technologiesOrder, result);
    }
}