package com.pragma.arquetipobootcamp2024.adapters.driving.http.controller;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.AddBootcampRequest;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.BootcampResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.BootcampsResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.IBootcampRequestMapper;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.IBootcampResponseMapper;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.ICapacityResponseMapper;
import com.pragma.arquetipobootcamp2024.domain.api.IBootcampServicePort;
import com.pragma.arquetipobootcamp2024.domain.api.ICapacityServicePort;
import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/bootcamp")
@RequiredArgsConstructor
public class BootcampRestControllerAdapter {

    private final IBootcampServicePort bootcampServicePort;
    private final IBootcampRequestMapper bootcampRequestMapper;
    private final IBootcampResponseMapper bootcampResponseMapper;

    @PostMapping("/")
    public ResponseEntity<Void> addBootcamp(@RequestBody @Valid AddBootcampRequest addBootcampRequest){
        bootcampServicePort.saveBootcamp(bootcampRequestMapper.addRequestToBootcamp(addBootcampRequest));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/search/{name}")
    public ResponseEntity<BootcampResponse> getBootcamp(@PathVariable String name){
        return ResponseEntity.ok(bootcampResponseMapper
                .toBootcampResponse(bootcampServicePort.getBootcamp(name)));
    }

    @GetMapping("/search")
    public ResponseEntity<BootcampsResponse> getAllBootcamps(@RequestParam Integer page,
                                                             @RequestParam Integer size,
                                                             @RequestParam boolean asc,
                                                             @RequestParam String type){

        List<Object> responseServicePort = bootcampServicePort.getAllBootcamps(page, size, asc, type);
        List<Bootcamp> bootcamps = (List<Bootcamp>) responseServicePort.get(0);
        List<BootcampResponse> bootcampResponseList = bootcampResponseMapper.toBootcampResponseList(bootcamps);
        int totalPages = (int) responseServicePort.get(1);
        BootcampsResponse bootcampsResponse = new BootcampsResponse(totalPages, bootcampResponseList);
        return ResponseEntity.ok(bootcampsResponse);
    }
}
