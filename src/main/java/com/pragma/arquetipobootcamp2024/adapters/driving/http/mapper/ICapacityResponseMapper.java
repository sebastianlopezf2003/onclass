package com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.CapacityResponse;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "Spring")
public interface ICapacityResponseMapper {
    @Mapping(source = "capacityId", target = "capacityId")
    @Mapping(source = "capacityName", target = "capacityName")
    @Mapping(source = "capacityDescription", target = "capacityDescription")
    @Mapping(source = "capacityTechnologies", target = "capacityTechnologies")
    CapacityResponse toCapacityResponse(Capacity capacity);
    List<CapacityResponse> toCapacityResponseList(List<Capacity> capacities);
}
