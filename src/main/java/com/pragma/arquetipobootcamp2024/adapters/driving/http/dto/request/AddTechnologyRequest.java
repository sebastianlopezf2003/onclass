package com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public record AddTechnologyRequest(
        @NotBlank(message = "Field 'name' cannot be empty")
        @Size(max = 50, message = "Field 'name' cannot be longer than 50 characters")
        String name,

        @NotBlank(message = "Field 'description' cannot be empty")
        @Size(max = 90, message = "Field 'description' cannot be longer than 50 characters")
        String description
) {}
