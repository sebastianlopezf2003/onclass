package com.pragma.arquetipobootcamp2024.adapters.driving.http.controller;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.TechnologiesResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.TechnologyResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.AddTechnologyRequest;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.ITechnologyRequestMapper;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.ITechnologyResponseMapper;
import com.pragma.arquetipobootcamp2024.configuration.security.utils.JwtUtils;
import com.pragma.arquetipobootcamp2024.domain.api.ITechnologyServicePort;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/technology")
@RequiredArgsConstructor
public class TechnologyRestControllerAdapter {
    private final ITechnologyServicePort technologyServicePort;
    private final ITechnologyRequestMapper technologyRequestMapper;
    private final ITechnologyResponseMapper technologyResponseMapper;
    private final JwtUtils jwtUtils;

    @PostMapping("/")
    public ResponseEntity<Void> addTechnology(@RequestBody @Valid AddTechnologyRequest request) {
        technologyServicePort.saveTechnology(technologyRequestMapper.addRequestToTechnology(request));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/search/{name}")
    public ResponseEntity<TechnologyResponse> getTechnology(@PathVariable String name) {
        return ResponseEntity.ok(technologyResponseMapper.
                toTechnologyResponse(technologyServicePort.getTechnology(name)));
    }

    @GetMapping("/search")
    public ResponseEntity<TechnologiesResponse> getAllTechnologies(@RequestParam Integer page, @RequestParam Integer size, @RequestParam boolean asc) {
        List<Object> responseServicePort = technologyServicePort.getAllTechnologies(page, size, asc);
        List<Technology> technologies = (List<Technology>) responseServicePort.get(0);
        List<TechnologyResponse> technologyResponseList = technologyResponseMapper.toTechnologyResponseList(technologies);
        int totalPages = (int) responseServicePort.get(1);
        TechnologiesResponse technologiesResponse = new TechnologiesResponse(totalPages, technologyResponseList);
        return ResponseEntity.ok(technologiesResponse);
    }
}
