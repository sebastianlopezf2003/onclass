package com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class TechnologiesResponse {
    private final int totalPages;
    private final List<TechnologyResponse> technologyResponseList;
}
