package com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.AddCapacityRequest;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "Spring")
public interface ICapacityRequestMapper {
    @Mapping(target = "capacityId", ignore = true)
    @Mapping(target = "capacity.capacityName", constant = "capacityName")
    @Mapping(target = "capacity.capacityDescription", constant = "capacityDescription")
    Capacity addRequestToCapacity(AddCapacityRequest addCapacityRequest);
}
