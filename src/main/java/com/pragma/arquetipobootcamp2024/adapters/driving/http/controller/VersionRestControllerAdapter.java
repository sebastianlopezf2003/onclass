package com.pragma.arquetipobootcamp2024.adapters.driving.http.controller;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.AddVersionRequest;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.VersionResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.IVersionRequestMapper;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.IVersionResponseMapper;
import com.pragma.arquetipobootcamp2024.domain.api.IVersionServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/version")
@RequiredArgsConstructor
public class VersionRestControllerAdapter {

    private final IVersionServicePort versionServicePort;
    private final IVersionRequestMapper versionRequestMapper;
    private final IVersionResponseMapper versionResponseMapper;

    @PostMapping("/")
    public ResponseEntity<Void> addVersion(@RequestBody @Valid AddVersionRequest addVersionRequest){
        versionServicePort.saveVersion(versionRequestMapper.addRequestToVersion(addVersionRequest));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/search")
    public ResponseEntity<List<VersionResponse>> getAllVersions(@RequestParam Integer page,
                                                                @RequestParam Integer size,
                                                                @RequestParam boolean asc,
                                                                @RequestParam String orderBy){
        return ResponseEntity.ok(versionResponseMapper.
                toVersionResponseList(versionServicePort.getAllVersions(page, size, asc, orderBy)));
    }

    @GetMapping("/search/{bootcampName}")
    public ResponseEntity<List<VersionResponse>> getAllBootcampVersions(@PathVariable String bootcampName){
        return ResponseEntity.ok(versionResponseMapper.toVersionResponseList(versionServicePort.
                getAllBootcampVersions(bootcampName)));
    }
}
