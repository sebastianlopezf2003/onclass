package com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class BootcampResponse {
    Long bootcampId;
    String bootcampName;
    String bootcampDescription;
    List<CapacityIdResponse> bootcampCapacities;
}
