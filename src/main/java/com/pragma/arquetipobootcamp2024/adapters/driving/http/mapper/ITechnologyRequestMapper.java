package com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.AddTechnologyRequest;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ITechnologyRequestMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "technology.name", constant = "name")
    @Mapping(target = "technology.description", constant = "description")
    Technology addRequestToTechnology(AddTechnologyRequest addTechnologyRequest);

}
