package com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public record AddVersionRequest(
        @NotNull
        BootcampIdRequest bootcamp,
        @NotNull
        Long maxQuota,
        @NotNull
        LocalDate startDate,
        @NotNull
        LocalDate endDate
) {
}
