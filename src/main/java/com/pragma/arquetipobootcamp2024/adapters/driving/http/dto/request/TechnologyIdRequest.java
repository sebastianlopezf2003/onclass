package com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request;

public record TechnologyIdRequest(Long id) {}

