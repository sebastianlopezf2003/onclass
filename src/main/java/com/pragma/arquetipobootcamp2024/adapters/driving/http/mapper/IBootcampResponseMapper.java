package com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.BootcampResponse;
import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "Spring")
public interface IBootcampResponseMapper {
    @Mapping(source = "bootcampId", target = "bootcampId")
    @Mapping(source = "bootcampName", target = "bootcampName")
    @Mapping(source = "bootcampDescription", target = "bootcampDescription")
    @Mapping(source = "bootcampCapacities", target = "bootcampCapacities")
    BootcampResponse toBootcampResponse(Bootcamp bootcamp);
    List<BootcampResponse> toBootcampResponseList(List<Bootcamp> bootcamps);
}
