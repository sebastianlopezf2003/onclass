package com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
@JsonPropertyOrder({"capacityId", "capacityName", "capacityTechnologies"})
public class CapacityIdResponse {
    @JsonProperty("capacityId")
    Long capacityId;
    @JsonProperty("capacityName")
    String capacityName;
    @JsonProperty
    List<TechnologyIdResponse> capacityTechnologies;
}
