package com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.AddVersionRequest;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.BootcampIdRequest;
import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import com.pragma.arquetipobootcamp2024.domain.model.Version;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "Spring")
public interface IVersionRequestMapper {

    @Mapping(target = "versionId", ignore = true)
    Version addRequestToVersion(AddVersionRequest addVersionRequest);
    @Mapping(target = "bootcampId", source = "id")
    Bootcamp bootcampIdRequestToBootcamp(BootcampIdRequest bootcampIdRequest);
}
