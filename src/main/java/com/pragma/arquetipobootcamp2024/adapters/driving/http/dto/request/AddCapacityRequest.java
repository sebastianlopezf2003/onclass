package com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

public record AddCapacityRequest(
        @NotBlank(message = "Field 'name' cannot be empty")
        @Size(max = 50, message = "Field 'name' cannot be longer than 50 characters")
        String capacityName,

        @NotBlank(message = "Field 'description' cannot be empty")
        @Size(max = 90, message = "Field 'description' cannot be longer than 90 characters")
        String capacityDescription,

        List<TechnologyIdRequest> capacityTechnologies
) {}
