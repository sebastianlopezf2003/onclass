package com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.AddBootcampRequest;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.CapacityIdRequest;
import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "Spring")
public interface IBootcampRequestMapper {
    @Mapping(target = "bootcampId", ignore = true)
    @Mapping(target = "bootcamp.bootcampName", constant = "bootcampName")
    @Mapping(target = "bootcamp.bootcampDescription", constant = "bootcampDescription")
    @Mapping(target = "bootcamp.bootcampCapacities", constant = "bootcampCapacities")
    Bootcamp addRequestToBootcamp(AddBootcampRequest addBootcampRequest);
    @Mapping(target = "capacityId", source = "id")
    Capacity capacityIdRequestToCapacity(CapacityIdRequest capacityIdRequest);
}
