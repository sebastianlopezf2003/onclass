package com.pragma.arquetipobootcamp2024.adapters.driving.http.controller;

import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request.AddCapacityRequest;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.CapacitiesResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response.CapacityResponse;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.ICapacityRequestMapper;
import com.pragma.arquetipobootcamp2024.adapters.driving.http.mapper.ICapacityResponseMapper;
import com.pragma.arquetipobootcamp2024.domain.api.ICapacityServicePort;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/capacity")
@RequiredArgsConstructor
public class CapacityRestControllerAdapter {
    private final ICapacityServicePort capacityServicePort;
    private final ICapacityRequestMapper capacityRequestMapper;
    private final ICapacityResponseMapper capacityResponseMapper;

    @PostMapping("/")
    public ResponseEntity<Void> addCapacity(@RequestBody @Valid AddCapacityRequest addCapacityRequest){
        capacityServicePort.saveCapacity(capacityRequestMapper.addRequestToCapacity(addCapacityRequest));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/search/{name}")
    public ResponseEntity<CapacityResponse> getCapacity(@PathVariable String name){
        return ResponseEntity.ok(capacityResponseMapper
                .toCapacityResponse(capacityServicePort.getCapacity(name)));
    }

    @GetMapping("/search")
    public ResponseEntity<CapacitiesResponse> getAllCapacities(@RequestParam Integer page,
                                                                   @RequestParam Integer size,
                                                                   @RequestParam boolean asc,
                                                                   @RequestParam String type){
        List<Object> responseServicePort = capacityServicePort.getAllCapacities(page, size, asc, type);
        List<Capacity> capacities = (List<Capacity>) responseServicePort.get(0);
        List<CapacityResponse> capacityResponseList = capacityResponseMapper.toCapacityResponseList(capacities);
        int totalPages = (int) responseServicePort.get(1);
        CapacitiesResponse capacitiesResponse = new CapacitiesResponse(totalPages, capacityResponseList);
        return ResponseEntity.ok(capacitiesResponse);
    }


}
