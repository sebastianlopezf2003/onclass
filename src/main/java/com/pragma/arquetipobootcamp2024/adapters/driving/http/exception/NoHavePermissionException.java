package com.pragma.arquetipobootcamp2024.adapters.driving.http.exception;

public class NoHavePermissionException extends RuntimeException{
    public NoHavePermissionException(){
        super();
    }
}
