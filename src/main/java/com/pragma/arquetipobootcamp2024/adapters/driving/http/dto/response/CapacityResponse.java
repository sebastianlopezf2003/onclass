package com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class CapacityResponse {
    private Long capacityId;
    private String capacityName;
    private String capacityDescription;
    private List<TechnologyIdResponse> capacityTechnologies;
}
