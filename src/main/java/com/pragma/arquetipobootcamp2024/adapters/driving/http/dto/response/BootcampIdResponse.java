package com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class BootcampIdResponse {
    @JsonProperty("bootcampId")
    Long bootcampId;
    @JsonProperty("bootcampName")
    String bootcampName;
}
