package com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class TechnologyIdResponse {
    @JsonProperty("Id")
    private final Long id;
    @JsonProperty("Name")
    private final String name;
}
