package com.pragma.arquetipobootcamp2024.adapters.driving.http.dto.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

public record AddBootcampRequest(
        @NotBlank(message = "Field 'name' cannot be empty")
        @Size(max = 50, message = "Field 'name' cannot be longer than 50 characters")
        String bootcampName,

        @NotBlank(message = "Field 'description' cannot be empty")
        @Size(max = 50, message = "Field 'description' cannot be longer than 50 characters")
        String bootcampDescription,

        List<CapacityIdRequest> bootcampCapacities
) {}
