package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.adapter;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.VersionEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.exception.ElementNoExistsException;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.exception.IncorrectDatesException;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper.IVersionEntityMapper;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.IBootcampRepository;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.IVersionRepository;
import com.pragma.arquetipobootcamp2024.domain.model.Version;
import com.pragma.arquetipobootcamp2024.domain.spi.IVersionPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;


import java.util.List;

@RequiredArgsConstructor
public class VersionAdapter implements IVersionPersistencePort {

    private final IVersionRepository versionRepository;
    private final IVersionEntityMapper versionEntityMapper;
    private final IBootcampRepository bootcampRepository;

    @Override
    public void saveVersion(Version version) {
        if(version.getStartDate().isAfter(version.getEndDate())){
            throw new IncorrectDatesException();
        }
        if(bootcampRepository.findById(version.getBootcamp().getBootcampId()).isEmpty()){
            throw new ElementNoExistsException(version.getBootcamp().getClass().getSimpleName(),
                    version.getBootcamp().getBootcampId());
        }
        versionRepository.save(versionEntityMapper.toEntity(version));
    }

    @Override
    public List<Version> getAllVersions(Integer page, Integer size, boolean asc, String orderBy) {
        Sort sort;
        Pageable pagination;
        if(orderBy.equals("bootcampName")){
            pagination = PageRequest.of(page, size);
            Page<VersionEntity> versionEntityPage = asc ? versionRepository.
                    findAllByOrderByBootcampBootcampNameAsc(pagination) : versionRepository.
                    findAllByOrderByBootcampBootcampNameDesc(pagination);
            return versionEntityMapper.toModelList(versionEntityPage.getContent());
        }else{
            sort = asc ? Sort.by(orderBy) : Sort.by(orderBy).descending();
            pagination = PageRequest.of(page, size, sort);
            return versionEntityMapper.toModelList(versionRepository.findAll(pagination).getContent());
        }
    }

    @Override
    public List<Version> getAllBootcampVersions(String bootcampName){
        return versionEntityMapper.toModelList(versionRepository.findAllByBootcampBootcampName(bootcampName));
    }
}
