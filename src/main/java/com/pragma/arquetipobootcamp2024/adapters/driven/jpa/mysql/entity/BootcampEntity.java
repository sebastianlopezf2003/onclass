package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "bootcamp")
@NoArgsConstructor
@Getter
@Setter
public class BootcampEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "bootcamp_id")
    private Long bootcampId;
    @Column (name = "bootcamp_name")
    private String bootcampName;
    @Column (name = "bootcamp_description")
    private String bootcampDescription;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(
            name = "bootcamp_capacity", joinColumns = @JoinColumn(name = "bootcamp_id", referencedColumnName = "bootcamp_id"),
            inverseJoinColumns = @JoinColumn(name = "capacity_id", referencedColumnName = "capacity_id")
    )
    private List<CapacityEntity> bootcampCapacities;

    @OneToMany(mappedBy = "bootcamp", cascade = CascadeType.ALL)
    private List<VersionEntity> versions;

    public BootcampEntity(Long bootcampId, String bootcampName, String bootcampDescription,
                          List<CapacityEntity> bootcampCapacities){
        this.bootcampId = bootcampId;
        this.bootcampName = bootcampName;
        this.bootcampDescription = bootcampDescription;
        this.bootcampCapacities = bootcampCapacities;
    }
}
