package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.adapter;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.BootcampEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.CapacityEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.exception.*;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper.IBootcampEntityMapper;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.IBootcampRepository;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.ICapacityRepository;
import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import com.pragma.arquetipobootcamp2024.domain.spi.IBootcampPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@RequiredArgsConstructor
public class BootcampAdapter implements IBootcampPersistencePort {
    private final IBootcampRepository bootcampRepository;
    private final IBootcampEntityMapper bootcampEntityMapper;
    private final ICapacityRepository capacityRepository;

    @Override
    public void saveBootcamp(Bootcamp bootcamp) {
        Set<Long> capacityIdSet = new HashSet<>();
        if(bootcampRepository.findByBootcampName(bootcamp.getBootcampName()).isPresent()){
            throw new ElementAlreadyExistsException(bootcamp.getClass().getSimpleName());
        }
        if(bootcamp.getBootcampCapacities().isEmpty() || bootcamp.getBootcampCapacities().size() > 4){
            throw new SizeCapacitiesException();
        }
        for (Capacity capacity : bootcamp.getBootcampCapacities()) {
            if (capacityRepository.findById(capacity.getCapacityId()).isEmpty()) {
                throw new ElementNoExistsException(capacity.getClass().getSimpleName(),
                        capacity.getCapacityId());
            }
            if (capacityIdSet.contains(capacity.getCapacityId())) {
                throw new DuplicateElementException(capacity.getClass().getSimpleName(),
                        capacity.getCapacityId());
            }
            capacityIdSet.add(capacity.getCapacityId());
        }
        bootcampRepository.save(bootcampEntityMapper.toEntity(bootcamp));
    }

    @Override
    public Bootcamp getBootcamp(String name) {
        BootcampEntity bootcampEntity = bootcampRepository.findByBootcampName(name).orElseThrow(ElementNotFoundException::new);
        return bootcampEntityMapper.toModel(bootcampEntity);
    }

    @Override
    public List<Object> getAllBootcamps(Integer page, Integer size, boolean asc, String type) {
        Pageable pagination;
        Sort sort;
        Page<BootcampEntity> bootcampPage;
        int totalPages;
        List<Bootcamp> bootcamps;
        if (Objects.equals(type, "name")) {
            sort = asc ? Sort.by("bootcampName").ascending() :
                    Sort.by("bootcampName").descending();
            pagination = PageRequest.of(page, size, sort);
        } else if (Objects.equals(type, "quantity")) {
            pagination = PageRequest.of(page, size);
            bootcampPage = asc ? bootcampRepository.
                    bootcampsOrderByQuantityAsc(pagination) : bootcampRepository.
                    bootcampsOrderByQuantityDesc(pagination);
            bootcamps = bootcampEntityMapper.toModelList(bootcampPage.getContent());
            totalPages = bootcampPage.getTotalPages();
            return List.of(bootcamps, totalPages);
        } else {
            pagination = PageRequest.of(page, size);
        }

        bootcampPage = bootcampRepository.findAll(pagination);
        bootcamps = bootcampEntityMapper.toModelList(bootcampPage.getContent());
        totalPages = bootcampPage.getTotalPages();

        if (bootcamps.isEmpty()) {
            throw new NoDataFoundException();
        }

        return List.of(bootcamps, totalPages);
    }
}
