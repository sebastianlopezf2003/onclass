package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.BootcampEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface IBootcampRepository extends JpaRepository<BootcampEntity, Long> {
    Optional<BootcampEntity> findByBootcampName(String name);

    Page<BootcampEntity> findAll(Pageable pageable);

    @Query("SELECT b FROM BootcampEntity b ORDER BY SIZE(b.bootcampCapacities) DESC")
    Page<BootcampEntity> bootcampsOrderByQuantityDesc(Pageable pageable);

    @Query("SELECT b FROM BootcampEntity b ORDER BY SIZE(b.bootcampCapacities) ASC")
    Page<BootcampEntity> bootcampsOrderByQuantityAsc(Pageable pageable);
}
