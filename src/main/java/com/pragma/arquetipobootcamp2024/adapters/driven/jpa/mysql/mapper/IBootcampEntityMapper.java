package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.BootcampEntity;
import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "Spring")
public interface IBootcampEntityMapper {
    @Mapping(source = "bootcampId", target = "bootcampId")
    @Mapping(source = "bootcampName", target = "bootcampName")
    @Mapping(source = "bootcampDescription", target = "bootcampDescription")
    Bootcamp toModel(BootcampEntity bootcampEntity);
    @Mapping(source = "bootcampId", target = "bootcampId")
    @Mapping(source = "bootcampName", target = "bootcampName")
    @Mapping(source = "bootcampDescription", target = "bootcampDescription")
    @Mapping(source = "bootcampCapacities", target = "bootcampCapacities")
    BootcampEntity toEntity(Bootcamp bootcamp);
    List<Bootcamp> toModelList(List<BootcampEntity> bootcampEntities);
}
