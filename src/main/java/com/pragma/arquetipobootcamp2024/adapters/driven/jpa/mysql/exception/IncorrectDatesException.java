package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.exception;

public class IncorrectDatesException extends RuntimeException{

    public IncorrectDatesException(){
        super();
    }
}
