package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.exception;

public class DuplicateElementException extends RuntimeException{

    public DuplicateElementException(String className, Long id){
        super("The " + className + " with id " +  id);
    }
}
