package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.CapacityEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Optional;

public interface ICapacityRepository extends JpaRepository<CapacityEntity, Long> {
    Optional<CapacityEntity> findByCapacityName(String name);

    Page<CapacityEntity> findAll(Pageable pageable);

    @Query("SELECT c FROM CapacityEntity c ORDER BY SIZE(c.capacityTechnologies) DESC")
    Page<CapacityEntity> capacitiesOrderByQuantityDesc(Pageable pageable);

    @Query("SELECT c FROM CapacityEntity c ORDER BY SIZE(c.capacityTechnologies) ASC")
    Page<CapacityEntity> capacitiesOrderByQuantityAsc(Pageable pageable);
}