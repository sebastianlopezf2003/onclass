package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.CapacityEntity;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import java.util.List;

@Mapper(componentModel = "Spring")
public interface ICapacityEntityMapper {
    @Mapping(source = "capacityId", target = "capacityId")
    @Mapping(source = "capacityName", target = "capacityName")
    @Mapping(source = "capacityDescription", target = "capacityDescription")
    Capacity toModel(CapacityEntity capacityEntity);
    @Mapping(source = "capacityId", target = "capacityId")
    @Mapping(source = "capacityName", target = "capacityName")
    @Mapping(source = "capacityDescription", target = "capacityDescription")
    @Mapping(source = "capacityTechnologies", target = "capacityTechnologies")
    CapacityEntity toEntity(Capacity capacity);
    List<Capacity> toModelList(List<CapacityEntity> capacitiesEntity);
}
