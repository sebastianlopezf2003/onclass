package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "technology")
@NoArgsConstructor
@Getter
@Setter

public class TechnologyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @ManyToMany(mappedBy = "capacityTechnologies")
    private List<CapacityEntity> capacities;

    public TechnologyEntity(Long id, String name, String description){
        this.id = id;
        this.name = name;
        this.description = description;
    }
}
