package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity;


import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "capacity")
@NoArgsConstructor
@Getter
@Setter
public class CapacityEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "capacity_id")
    private Long capacityId;
    @Column (name = "capacity_name")
    private String capacityName;
    @Column (name = "capacity_description")
    private String capacityDescription;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(
            name = "capacity_technology", joinColumns = @JoinColumn(name = "capacity_id", referencedColumnName = "capacity_id"),
            inverseJoinColumns = @JoinColumn(name = "technology_id", referencedColumnName = "id")
    )
    private List<TechnologyEntity> capacityTechnologies;
    @ManyToMany(mappedBy = "bootcampCapacities")
    private List<BootcampEntity> bootcamps;

    public CapacityEntity(Long capacityId, String capacityName, String capacityDescription,
                          List<TechnologyEntity> capacityTechnologies){
        this.capacityId = capacityId;
        this.capacityName = capacityName;
        this.capacityDescription = capacityDescription;
        this.capacityTechnologies = capacityTechnologies;
    }
}
