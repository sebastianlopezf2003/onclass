package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.VersionEntity;
import com.pragma.arquetipobootcamp2024.domain.model.Version;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface IVersionRepository extends JpaRepository<VersionEntity, Long> {
    Page<VersionEntity> findAll(Pageable pagination);

    Page<VersionEntity> findAllByOrderByBootcampBootcampNameAsc(Pageable pagination);

    Page<VersionEntity> findAllByOrderByBootcampBootcampNameDesc(Pageable pagination);

    List<VersionEntity> findAllByBootcampBootcampName(String bootcampName);
}

