package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.exception;

public class ElementNoExistsException extends RuntimeException{

    public ElementNoExistsException(String className, Long id){
        super("The " + className + " with id " + id);
    }
}
