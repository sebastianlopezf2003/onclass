package com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.adapter;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.entity.CapacityEntity;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.exception.*;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper.ICapacityEntityMapper;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.ICapacityRepository;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.ITechnologyRepository;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import com.pragma.arquetipobootcamp2024.domain.model.Technology;
import com.pragma.arquetipobootcamp2024.domain.spi.ICapacityPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.*;
import java.util.*;

@RequiredArgsConstructor
public class CapacityAdapter implements ICapacityPersistencePort {
    private final ICapacityRepository capacityRepository;
    private final ICapacityEntityMapper capacityEntityMapper;
    private final ITechnologyRepository technologyRepository;

    @Override
    public void saveCapacity(Capacity capacity) {
        Set<Long> technologyIdsSet = new HashSet<>();
        if (capacityRepository.findByCapacityName(capacity.getCapacityName()).isPresent()) {
            throw new ElementAlreadyExistsException(capacity.getClass().getSimpleName());
        }
        if (capacity.getCapacityTechnologies().size() < 3 || capacity.getCapacityTechnologies().size() > 20) {
            throw new SizeTechnologiesException();
        }
        for (Technology technology : capacity.getCapacityTechnologies()) {
            if (technologyRepository.findById(technology.getId()).isEmpty()) {
                throw new ElementNoExistsException(technology.getClass().getSimpleName(),
                        technology.getId());
            }
            if (technologyIdsSet.contains(technology.getId())) {
                throw new DuplicateElementException(technology.getClass().getSimpleName(),
                        technology.getId());
            }
            technologyIdsSet.add(technology.getId());
        }
        capacityRepository.save(capacityEntityMapper.toEntity(capacity));
    }

    @Override
    public Capacity getCapacity(String name) {
        CapacityEntity capacity = capacityRepository.findByCapacityName(name).orElseThrow(ElementNotFoundException::new);
        return capacityEntityMapper.toModel(capacity);
    }

    @Override
    public List<Object> getAllCapacities(Integer page, Integer size, boolean asc, String type) {
        Pageable pagination;
        Sort sort;
        Page<CapacityEntity> capacityPage;
        List<Capacity> capacities;
        int totalPages;
        if (Objects.equals(type, "name")) {
            sort = asc ? Sort.by("capacityName").ascending() :
                    Sort.by("capacityName").descending();
            pagination = PageRequest.of(page, size, sort);
        } else if (Objects.equals(type, "quantity")) {
            pagination = PageRequest.of(page, size);
            capacityPage =  asc ? capacityRepository.
                    capacitiesOrderByQuantityAsc(pagination) : capacityRepository.
                    capacitiesOrderByQuantityDesc(pagination);
            capacities = capacityEntityMapper.toModelList(capacityPage.getContent());
            totalPages = capacityPage.getTotalPages();
            return List.of(capacities, totalPages);
        } else {
            pagination = PageRequest.of(page, size);
        }

        capacityPage = capacityRepository.findAll(pagination);
        capacities = capacityEntityMapper.toModelList(capacityPage.getContent());
        if (capacities.isEmpty()) {
            throw new NoDataFoundException();
        }
        totalPages = capacityPage.getTotalPages();

        return List.of(capacities, totalPages);
    }
}