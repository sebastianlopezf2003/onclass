package com.pragma.arquetipobootcamp2024.domain.spi;

import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;

import java.util.List;

public interface IBootcampPersistencePort {
    void saveBootcamp(Bootcamp bootcamp);
    Bootcamp getBootcamp(String name);
    List<Object> getAllBootcamps(Integer page, Integer size, boolean asc, String type);
}
