package com.pragma.arquetipobootcamp2024.domain.model;

import java.util.List;

public class Capacity {
    private Long capacityId;
    private String capacityName;
    private String capacityDescription;
    private List<Technology> capacityTechnologies;

    public Capacity(){}

    public Capacity (Long capacityId, String capacityName, String capacityDescription, List<Technology> capacityTechnologies) {
        this.capacityId = capacityId;
        this.capacityName = capacityName;
        this.capacityDescription = capacityDescription;
        this.capacityTechnologies = capacityTechnologies;
    }

    public Long getCapacityId(){ return capacityId;}
    public String getCapacityName(){ return capacityName;}
    public String getCapacityDescription(){ return capacityDescription;}
    public List<Technology> getCapacityTechnologies() { return capacityTechnologies;}

    public void setCapacityId(Long capacityId) {this.capacityId = capacityId;}
    public void setCapacityName(String capacityName) {this.capacityName = capacityName;}
    public void setCapacityDescription(String capacityDescription) {this.capacityDescription = capacityDescription;}
    public void setCapacityTechnologies(List<Technology> capacityTechnologies) {this.capacityTechnologies = capacityTechnologies;}

}
