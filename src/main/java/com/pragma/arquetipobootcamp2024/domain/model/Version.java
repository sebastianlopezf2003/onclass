package com.pragma.arquetipobootcamp2024.domain.model;

import java.time.LocalDate;

public class Version {

    private Long versionId;
    private Bootcamp bootcamp;
    private Long maxQuota;
    private LocalDate startDate;
    private LocalDate endDate;

    public Version(Long versionId, Bootcamp bootcamp, Long maxQuota, LocalDate startDate, LocalDate endDate){
        this.versionId = versionId;
        this.bootcamp = bootcamp;
        this.maxQuota = maxQuota;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Long getVersionId() { return versionId;}
    public Bootcamp getBootcamp() { return bootcamp;}
    public Long getMaxQuota() { return maxQuota;}
    public LocalDate getStartDate() { return startDate;}
    public LocalDate getEndDate() { return endDate;}

    public void setVersionId(Long versionId) { this.versionId = versionId;}
    public void setBootcamp(Bootcamp bootcamp) { this.bootcamp = bootcamp;}
    public void setMaxQuota(Long maxQuota) { this.maxQuota = maxQuota;}
    public void setStartDate(LocalDate startDate) { this.startDate = startDate;}
    public void setEndDate(LocalDate endDate) { this.endDate = endDate;}
}
