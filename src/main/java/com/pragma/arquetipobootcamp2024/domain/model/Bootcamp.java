package com.pragma.arquetipobootcamp2024.domain.model;

import java.util.List;

public class Bootcamp {
    private Long bootcampId;
    private String bootcampName;
    private String bootcampDescription;
    private List<Capacity> bootcampCapacities;

    public Bootcamp(){}

    public Bootcamp(Long bootcampId, String bootcampName, String bootcampDescription, List<Capacity> bootcampCapacities){
        this.bootcampId = bootcampId;
        this.bootcampName = bootcampName;
        this.bootcampDescription = bootcampDescription;
        this.bootcampCapacities = bootcampCapacities;
    }

    public Long getBootcampId(){ return bootcampId;}
    public String getBootcampName(){ return bootcampName;}
    public String getBootcampDescription(){ return bootcampDescription;}
    public List<Capacity> getBootcampCapacities() { return bootcampCapacities;}

    public void setBootcampId(Long bootcampId) {this.bootcampId = bootcampId;}
    public void setBootcampName(String bootcampName) {this.bootcampName = bootcampName;}
    public void setBootcampDescription(String bootcampDescription) {this.bootcampDescription = bootcampDescription;}
    public void setBootcampCapacities(List<Capacity> bootcampCapacities) {this.bootcampCapacities = bootcampCapacities;}
}
