package com.pragma.arquetipobootcamp2024.domain.api;

import com.pragma.arquetipobootcamp2024.domain.model.Technology;

import java.util.List;
import java.util.Objects;

public interface ITechnologyServicePort {
    void saveTechnology(Technology technology);
    Technology getTechnology(String name);
    List<Object> getAllTechnologies(Integer page, Integer size, boolean asc);
}
