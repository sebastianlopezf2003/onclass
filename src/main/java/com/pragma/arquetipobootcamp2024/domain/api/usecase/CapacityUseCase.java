package com.pragma.arquetipobootcamp2024.domain.api.usecase;

import com.pragma.arquetipobootcamp2024.domain.api.ICapacityServicePort;
import com.pragma.arquetipobootcamp2024.domain.model.Capacity;
import com.pragma.arquetipobootcamp2024.domain.spi.ICapacityPersistencePort;

import java.util.List;

public class CapacityUseCase implements ICapacityServicePort {

    private final ICapacityPersistencePort capacityPersistencePort;

    public CapacityUseCase(ICapacityPersistencePort capacityPersistencePort){
        this.capacityPersistencePort = capacityPersistencePort;
    }

    @Override
    public void saveCapacity(Capacity capacity){
        capacityPersistencePort.saveCapacity(capacity);
    }

    @Override
    public Capacity getCapacity(String nameCapacity){
        return capacityPersistencePort.getCapacity(nameCapacity);
    }

    @Override
    public List<Object> getAllCapacities(Integer page, Integer size, boolean asc, String type){
        return capacityPersistencePort.getAllCapacities(page,size, asc, type);
    }
}
