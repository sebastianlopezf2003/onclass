package com.pragma.arquetipobootcamp2024.domain.api;

import com.pragma.arquetipobootcamp2024.domain.model.Capacity;

import java.util.List;

public interface ICapacityServicePort {
    void saveCapacity(Capacity capacity);
    Capacity getCapacity(String capacityName);
    List<Object> getAllCapacities(Integer page, Integer size, boolean asc, String type);
}
