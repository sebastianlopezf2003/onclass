package com.pragma.arquetipobootcamp2024.domain.api;

import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;

import java.util.List;

public interface IBootcampServicePort {
    void saveBootcamp(Bootcamp bootcamp);
    Bootcamp getBootcamp(String name);
    List<Object> getAllBootcamps(Integer page, Integer size, boolean asc, String type);
}
