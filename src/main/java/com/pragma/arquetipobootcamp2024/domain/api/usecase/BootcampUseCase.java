package com.pragma.arquetipobootcamp2024.domain.api.usecase;

import com.pragma.arquetipobootcamp2024.domain.api.IBootcampServicePort;
import com.pragma.arquetipobootcamp2024.domain.model.Bootcamp;
import com.pragma.arquetipobootcamp2024.domain.spi.IBootcampPersistencePort;

import java.util.List;

public class BootcampUseCase implements IBootcampServicePort {

    private final IBootcampPersistencePort bootcampPersistencePort;

    public BootcampUseCase(IBootcampPersistencePort bootcampPersistencePort) {
        this.bootcampPersistencePort = bootcampPersistencePort;
    }

    @Override
    public void saveBootcamp(Bootcamp bootcamp){
        bootcampPersistencePort.saveBootcamp(bootcamp);
    }

    @Override
    public Bootcamp getBootcamp(String name) {
        return bootcampPersistencePort.getBootcamp(name);
    }

    @Override
    public List<Object> getAllBootcamps(Integer page, Integer size, boolean asc, String type) {
        return bootcampPersistencePort.getAllBootcamps(page, size, asc, type);
    }
}
