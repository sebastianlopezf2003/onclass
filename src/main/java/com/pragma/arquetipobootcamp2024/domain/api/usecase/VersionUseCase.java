package com.pragma.arquetipobootcamp2024.domain.api.usecase;

import com.pragma.arquetipobootcamp2024.domain.api.IVersionServicePort;
import com.pragma.arquetipobootcamp2024.domain.model.Version;
import com.pragma.arquetipobootcamp2024.domain.spi.IVersionPersistencePort;

import java.util.List;

public class VersionUseCase implements IVersionServicePort {

    private final IVersionPersistencePort versionPersistencePort;

    public VersionUseCase(IVersionPersistencePort versionPersistencePort) {
        this.versionPersistencePort = versionPersistencePort;
    }

    @Override
    public void saveVersion(Version version) {
        versionPersistencePort.saveVersion(version);
    }

    @Override
    public List<Version> getAllVersions(Integer page, Integer size, boolean asc, String orderBy) {
        return versionPersistencePort.getAllVersions(page,size, asc, orderBy);
    }

    @Override
    public List<Version> getAllBootcampVersions(String bootcampName){
        return versionPersistencePort.getAllBootcampVersions(bootcampName);
    }
}
