package com.pragma.arquetipobootcamp2024.domain.api;

import com.pragma.arquetipobootcamp2024.domain.model.Version;
import com.pragma.arquetipobootcamp2024.domain.spi.IVersionPersistencePort;

import java.util.List;

public interface IVersionServicePort {
    void saveVersion(Version version);
    List<Version> getAllVersions(Integer page, Integer size, boolean asc, String orderBy);
    List<Version> getAllBootcampVersions(String bootcampName);
}
