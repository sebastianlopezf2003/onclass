package com.pragma.arquetipobootcamp2024.configuration.exceptionhandler;

public class ExceptionArgumentResponse {
    private final String fieldName;
    private final String message;

    public ExceptionArgumentResponse(String fieldName, String message) {
        this.fieldName = fieldName;
        this.message = message;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getMessage() {
        return message; }
}

