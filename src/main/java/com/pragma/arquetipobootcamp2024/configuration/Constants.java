package com.pragma.arquetipobootcamp2024.configuration;

public class Constants {
    private Constants(){
        throw new IllegalStateException("utility class");
    }

    public static final String NO_DATA_FOUND_EXCEPTION_MESSAGE = "No data was found in the database";
    public static final String ELEMENT_NOT_FOUND_EXCEPTION_MESSAGE = "The element indicated does not exist";
    public static final String EMPTY_FIELD_EXCEPTION_MESSAGE = "Field %s can not be empty";
    public static final String NEGATIVE_NOT_ALLOWED_EXCEPTION_MESSAGE = "Field %s can not receive negative values";
    public static final String SIZE_TECHNOLOGIES_EXCEPTION_MESSAGE = "The capacity have be between 3 and 20 technologies";
    public static final String SIZE_CAPACITIES_EXCEPTION_MESSAGE = "The bootcamp have be between 1 and 4 technologies";
    public static final String INCORRECT_DATES_EXCEPTION_MESSAGE = "The end date must be after the start date";
    public static final String ELEMENT_NO_EXISTS_EXCEPTION_MESSAGE = "%s does not exists";
    public static final String ELEMENT_ALREADY_EXISTS_EXCEPTION_MESSAGE = "The %s you want to create already exists";
    public static final String DUPLICATE_ELEMENT_EXCEPTION_MESSAGE = "%s is duplicate";
    public static final String NO_HAVE_PERMISSION_EXCEPTION_MESSAGE = "The rol do not have permission";
}
