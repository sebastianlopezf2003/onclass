package com.pragma.arquetipobootcamp2024.configuration;

import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.adapter.BootcampAdapter;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.adapter.CapacityAdapter;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.adapter.TechnologyAdapter;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.adapter.VersionAdapter;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper.IBootcampEntityMapper;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper.ICapacityEntityMapper;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper.ITechnologyEntityMapper;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.mapper.IVersionEntityMapper;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.IBootcampRepository;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.ICapacityRepository;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.ITechnologyRepository;
import com.pragma.arquetipobootcamp2024.adapters.driven.jpa.mysql.repository.IVersionRepository;
import com.pragma.arquetipobootcamp2024.domain.api.IBootcampServicePort;
import com.pragma.arquetipobootcamp2024.domain.api.ICapacityServicePort;
import com.pragma.arquetipobootcamp2024.domain.api.ITechnologyServicePort;
import com.pragma.arquetipobootcamp2024.domain.api.IVersionServicePort;
import com.pragma.arquetipobootcamp2024.domain.api.usecase.BootcampUseCase;
import com.pragma.arquetipobootcamp2024.domain.api.usecase.CapacityUseCase;
import com.pragma.arquetipobootcamp2024.domain.api.usecase.TechnologyUseCase;
import com.pragma.arquetipobootcamp2024.domain.api.usecase.VersionUseCase;
import com.pragma.arquetipobootcamp2024.domain.spi.IBootcampPersistencePort;
import com.pragma.arquetipobootcamp2024.domain.spi.ICapacityPersistencePort;
import com.pragma.arquetipobootcamp2024.domain.spi.ITechnologyPersistencePort;
import com.pragma.arquetipobootcamp2024.domain.spi.IVersionPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {
    private final ITechnologyRepository technologyRepository;
    private final ITechnologyEntityMapper technologyEntityMapper;
    private final ICapacityRepository capacityRepository;
    private final ICapacityEntityMapper capacityEntityMapper;
    private final IBootcampRepository bootcampRepository;
    private final IBootcampEntityMapper bootcampEntityMapper;
    private final IVersionRepository versionRepository;
    private final IVersionEntityMapper versionEntityMapper;

    @Bean
    public ITechnologyPersistencePort technologyPersistencePort() {
        return new TechnologyAdapter(technologyRepository, technologyEntityMapper);
    }
    @Bean
    public ITechnologyServicePort technologyServicePort() {
        return new TechnologyUseCase(technologyPersistencePort());
    }

    @Bean
    public ICapacityPersistencePort capacityPersistencePort() {
        return new CapacityAdapter(capacityRepository, capacityEntityMapper, technologyRepository);
    }
    @Bean
    public ICapacityServicePort capacityServicePort(){
        return new CapacityUseCase(capacityPersistencePort());
    }

    @Bean
    public IBootcampPersistencePort bootcampPersistencePort() {
        return new BootcampAdapter(bootcampRepository, bootcampEntityMapper, capacityRepository);
    }

    @Bean
    public IBootcampServicePort bootcampServicePort() { return new BootcampUseCase(bootcampPersistencePort());
    }

    @Bean
    public IVersionPersistencePort versionPersistencePort() {
        return new VersionAdapter(versionRepository, versionEntityMapper, bootcampRepository);
    }

    @Bean
    public IVersionServicePort versionServicePort() { return new VersionUseCase(versionPersistencePort());
    }
}
